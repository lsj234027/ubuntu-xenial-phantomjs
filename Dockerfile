FROM ubuntu:xenial

USER root

COPY phantomjs/. /usr/local/phantomjs/

RUN apt-get -y update | chmod 777 /usr/local/phantomjs/bin/phantomjs
RUN apt-get -y install libfontconfig1 libjpeg8 libssl1.0.0 libicu55 libxml2 libxslt1-dev libhyphen0

